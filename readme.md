first you have to add the file to git

	git add (filename)

or everything that you've changed

	git add .

---

Then give it a message

	git commit -m "message"

    (keep quotes just replace message with your message)

---

Then finally upload it everything you've added

	git push
