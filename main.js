const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

let win;

function createWindow() {
  win = new BrowserWindow({width: 384, height: 256, frame: false, 'minWidth': 384, 'minHeight': 256});
  global.win = win
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'controller.html'),
    protocol: 'file:',
    slashes: true
  }));

  // win.webContents.openDevTools();

  win.on('closed', () => {
    win = null
  });

}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
