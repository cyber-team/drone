const funcs = [
  'takeoff',
  'land',
  'forward',
  'back',
  'up',
  'down',
  'stop'
];

const spd = 0.4;

class Helper {
  constructor(client) {
    this.client = client;
    this.commands = [];
    this.total = 0;
  }

  add(command, time) {
    console.debug(command, funcs)
    if (funcs.includes(command)) {
      this.commands.push([command, time]);
    } else {
      throw "Invalid command: " + command;
    }
  }

  run() {
    for (i=0; i< this.commands.length; i++) {
      let cmd = this.commands[i];
      var client = this.client;
      client.after(0 + cmd[1], function() {
        try {
          client[cmd[0]](spd);
        } catch {
          client[cmd[0]]()
        }
      })
      this.total += cmd[1];
    }
  }
}

var exports = module.exports = Helper
