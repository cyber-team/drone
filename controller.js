const electron = require('electron');

const arDrone = require('ar-drone');
const client  = arDrone.createClient();
const Drone = require('./drone');

const fs = require('fs');

global.win = electron.remote.getGlobal('win')

const speed      = 0.2;
const shiftSpeed = 0.1;

const altScale = 1.5;
const altWarn = 1;

const debugEchoKeys = true;

const controls = {
  Shift: 'shift',
  w    : 'forward',
  a    : 'left',
  s    : 'backward',
  d    : 'right',
  q    : 'yawleft',
  e    : 'yawright',
  c    : 'down',
  " "  : 'up',
  y    : 'power',
  '1'  : 'rotateto',
  '`'  : 'togglecontrols'
}

var inputs = {}

for (control in controls) {
  inputs[controls[control]] = false;
}

var switches = {
  'droneState': function(state) {
    if (state) {
      client.takeoff();
    } else {
      client.land()
    }
  }
}

function runScript(file) {
  script = require('./scripts/' + file);
  drone = new Drone(client);
  script(drone)
}

function loadScripts() {
  var list = document.getElementById('script-list');
  fs.readdir('./scripts/', function(err, items) {
    for (var i=0; i<items.length; i++) {
      var item = items[i];
      var id = `btn-${i}`
      var li = document.createElement('li');
      var btn = document.createElement('span');
      btn.className = `btn`;
      btn.id = id;
      console.log(btn, item);
      list.appendChild(li);
      li.appendChild(btn);
      // document.getElementById(id).innerHtml = item;
      btn.textContent = item
      btn.onclick = function() {runScript(item.split('.')[0])};
    }
  })
}

function warn() {
  document.body.className = 'warning';
}

function warnOff() {
  document.body.className = '';
}

var navdata;
var lastInputs = inputs;
var arrow;
var vertIndicator;
var horIndicator;
var altIndicator;
var altIndicatorSpan;

var rotating = 0;

function getDir(i) {
  if (i.forward) {
    if (i.left) {
      return 'forwardleft'
    } else if (i.right) {
      return 'forwardright'
    } else {
      return 'forward'
    }
  } else if (i.backward) {
    if (i.left) {
      return 'backleft'
    } else if (i.right) {
      return 'backright'
    } else {
      return 'backward'
    }
  } else if (i.right) {
    return 'right'
  } else if (i.left) {
    return 'left'
  } else {
    return ''
  }
}

function rotateTo(deg) {
  client.clockwise(0.8)
  rotating = 1
  client.on('navdata', function(data) {
    var yaw = data.demo.rotation.yaw;
    if (deg + 5 > yaw > deg - 5) {
      client.clockwise(0);
      rotating = false;
    }
  })
}

function main() {

  console.log('old', lastInputs)
  console.log('new', inputs);

  // for (input in inputs) {
  //   let val = inputs[input];
  //   if (val) {
  //     arrow.classList.add(input);
  //   } else {
  //     arrow.classList.remove(input);
  //   }
  // }
  var dir = getDir(inputs);
  var spd = inputs.shift ? shiftspeed : speed;
  var slow = spd / 2;
  var fast = spd * 2;
  arrow.classList = dir;

  if (inputs.runtest) {
    runScript(client, 'test');
  }

  if (inputs.power) {
    document.getElementById('btn-power').onclick();
  }

  if (inputs.rotateto) {
    rotateTo(90);
    return;
  }

  if (rotating) {return}

  // var rot = i.yawleft ? -1 : i.yawright ? 1 : 0;

  switch (dir) {
    case 'forward':
      client.front(spd);
      break;
    case 'forwardleft':
      client.front(spd);
      client.left(spd);
      break;
    case 'forwardright':
      client.front(spd);
      client.right(spd);
      break;
    case 'backward':
      client.back(spd);
      break;
    case 'backleft':
      client.back(spd);
      client.left(spd);
      break;
    case 'backright':
      client.back(spd);
      client.right(spd);
      break;
    case 'left':
      client.left(spd);
      break;
    case 'right':
      client.right(spd);
      break;
    case '':
      client.stop();
      break;
  }

  if (inputs.up) {
    client.up(spd);
    vertIndicator.className = 'up';
  } else if (inputs.down) {
    client.down(spd);
    vertIndicator.className = 'down';
  } else if (lastInputs.up || lastInputs.down) {
    client.up(0);
    client.down(0);
    vertIndicator.className = '';
  }

  if (inputs.yawleft) {
    client.counterClockwise(fast);
    horIndicator.className = 'left';
  } else if (inputs.yawright) {
    client.clockwise(fast);
    horIndicator.className = 'right';
  } else if (lastInputs.yawleft || lastInputs.yawright) {
    client.counterClockwise(0);
    client.clockwise(0);
    horIndicator.className = '';
  }

  lastInputs = Object.assign({}, inputs);
}

window.onload = function() {
  loadScripts();
  global.arrow = document.getElementById('arrow');
  global.vertIndicator = document.getElementById('vert-indicator');
  global.horIndicator = document.getElementById('hor-indicator');
  global.altIndicator = document.getElementById('alt-indicator');
  global.altIndicatorSpan = altIndicator.getElementsByTagName('span')[0];

  window.addEventListener('keydown', function(event) {
    if (debugEchoKeys) {console.log(event)}
    let key = event.key.toLowerCase()
    if (key in controls) {
      let control = controls[key];
      let state = inputs[control];
      if (!state) {
        inputs[control] = true;
        main()
      }
    }
  }, false)

  window.addEventListener('keyup', function(event) {
    let key = event.key.toLowerCase()
    if (key in controls) {
      let control = controls[key];
      let state = inputs[control];
      if (state) {
        inputs[control] = false;
        main()
      }
    }
  }, false)

  var buttons = document.getElementsByClassName('switch');
  for (i=0; i<buttons.length; i++) {
    var s = buttons[i];
    s.onclick = function(e) {
      console.log('clicked')
      let curstate = this.getAttribute('state') == 'on' ? true : false;
      let newstate = !curstate;
      this.setAttribute('state', newstate ? 'on' : 'off');
      console.log(this.getAttribute('value'), newstate);
      switches[this.getAttribute('value')](newstate);
    }
  }
  client.on('navdata', function(data) {
    let perc = ((data.demo.altitude / altScale)*100 || 0).toString() + '%';
    navdata = data;
    console.debug('navdata', navdata.demo, perc);
    altIndicatorSpan.style.height = perc;
  })
}
